# General info #

Simple spring boot application providing rest service to find matching holidays for 2 different countries from specific date
The application code assumes that holidays may differ for the same country based on year(for example some day is considered holiday only after year 1990) so it searches for match up to current year.
Unfortunately the holiday API used seems to be providing not accurate data in that regard(holidays in Poland are always the same even when you compare current ones with the ones from 100 years ago)

### How do I get set up? ###

Eeasiest options are - run as java application from IDE or install maven and execute "mvn spring-boot:run" in root directory. You can read more at http://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-running-your-application.html