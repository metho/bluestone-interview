package bluestone.interview.services;

import bluestone.interview.model.holidayapi.Holiday;
import bluestone.interview.model.responses.NextSameDateHoliday;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class HolidayMatcherServiceTest {

    @Autowired
    private HolidayMatcherService holidayMatcherService;


    @Test
    public void testMatchingDate() {
        Map<String, List<Holiday>> holidayInYearForCountry1 = createHolidayMapWithDatesAndNames("2015-03-03", "2015-04-03",
                "2015-05-03", "name1", "name2", "name3");

        Map<String, List<Holiday>> holidayInYearForCountry2 = createHolidayMapWithDatesAndNames("2015-04-03", "2015-07-03",
                "2015-08-03", "secondname1", "secondname2", "secondname3");

        LocalDate searchStartDate = LocalDate.parse("2015-01-01");
        Optional<NextSameDateHoliday> nextSameDateHoliday = holidayMatcherService.findNextSameDateHoliday(holidayInYearForCountry1, holidayInYearForCountry2, searchStartDate);

        assertTrue(nextSameDateHoliday.isPresent());
        NextSameDateHoliday nextSameDateHoliday1 = nextSameDateHoliday.get();
        assertEquals("2015-04-03", nextSameDateHoliday1.getDate());
        assertEquals("name2", nextSameDateHoliday1.getName1());
        assertEquals("secondname1", nextSameDateHoliday1.getName2());

    }


    @Test
    public void testMatchingDateNoMatchingHolidays() {

        Map<String, List<Holiday>> holidayInYearForCountry1 = createHolidayMapWithDatesAndNames("2015-03-03", "2015-04-03",
                "2015-05-03", "name1", "name2", "name3");

        Map<String, List<Holiday>> holidayInYearForCountry2 = createHolidayMapWithDatesAndNames("2015-06-03", "2015-07-03",
                "2015-08-03", "secondname1", "secondname2", "secondname3");

        LocalDate searchStartDate = LocalDate.parse("2015-01-01");
        Optional<NextSameDateHoliday> nextSameDateHoliday = holidayMatcherService.findNextSameDateHoliday(holidayInYearForCountry1, holidayInYearForCountry2, searchStartDate);

        assertFalse(nextSameDateHoliday.isPresent());
    }


    @Test
    public void testMatchingDateAccountForSearchStartDate() {
        Map<String, List<Holiday>> holidayInYearForCountry1 = createHolidayMapWithDatesAndNames("2015-03-03", "2015-04-03",
                "2015-05-03", "name1", "name2", "name3");

        Map<String, List<Holiday>> holidayInYearForCountry2 = createHolidayMapWithDatesAndNames("2015-04-03", "2015-05-03",
                "2015-08-03", "secondname1", "secondname2", "secondname3");

        LocalDate searchStartDate = LocalDate.parse("2015-04-05");
        Optional<NextSameDateHoliday> nextSameDateHoliday = holidayMatcherService.findNextSameDateHoliday(holidayInYearForCountry1, holidayInYearForCountry2, searchStartDate);

        assertTrue(nextSameDateHoliday.isPresent());
        NextSameDateHoliday nextSameDateHoliday1 = nextSameDateHoliday.get();
        assertEquals("2015-05-03", nextSameDateHoliday1.getDate());
        assertEquals("name3", nextSameDateHoliday1.getName1());
        assertEquals("secondname2", nextSameDateHoliday1.getName2());
    }


    private Map<String, List<Holiday>> createHolidayMapWithDatesAndNames(String date1, String date2, String date3, String name1, String name2, String name3) {
        Map<String, List<Holiday>> holidayInYearForCountry1 = new HashMap<>();
        holidayInYearForCountry1.put(date1, Collections.singletonList(new Holiday(name1, date1)));
        holidayInYearForCountry1.put(date2, Collections.singletonList(new Holiday(name2, date1)));
        holidayInYearForCountry1.put(date3, Collections.singletonList(new Holiday(name3, date1)));
        return holidayInYearForCountry1;
    }


}