package bluestone.interview.model.holidayapi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HolidayApiResponse {

    private String status;
    private Map<String, List<Holiday>> holidays;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public Map<String, List<Holiday>> getHolidays() {
        return holidays;
    }

    public void setHolidays(Map<String, List<Holiday>> holidays) {
        this.holidays = holidays;
    }
}
