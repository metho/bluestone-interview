package bluestone.interview.model.responses;

public class NextSameDateHoliday {

    public NextSameDateHoliday() {
    }

    public NextSameDateHoliday(String date, String name1, String name2) {
        this.date = date;
        this.name1 = name1;
        this.name2 = name2;
    }

    private String date;
    private String name1;
    private String name2;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }
}
