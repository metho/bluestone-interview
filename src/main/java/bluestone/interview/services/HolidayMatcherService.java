package bluestone.interview.services;

import bluestone.interview.model.holidayapi.Holiday;
import bluestone.interview.model.responses.NextSameDateHoliday;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class HolidayMatcherService {

    public Optional<NextSameDateHoliday> findNextSameDateHoliday(Map<String, List<Holiday>> holidayInYearForCountry1,
                                                                 Map<String, List<Holiday>> holidayInYearForCountry2, LocalDate searchStartDate) {
        List<String> matchingDates = retainAndSortMatchingDates(holidayInYearForCountry1, holidayInYearForCountry2, searchStartDate);
        if (matchingDates.isEmpty()) {
            return Optional.empty();
        }
        return createResponseObject(holidayInYearForCountry1, holidayInYearForCountry2, matchingDates);
    }

    private List<String> retainAndSortMatchingDates(Map<String, List<Holiday>> holidaysInYearForCountry1,
                                                    Map<String, List<Holiday>> holidaysInYearForCountry2, LocalDate searchStartDate) {
        Set<String> holidayDates1 = holidaysInYearForCountry1.keySet();
        Set<String> holidayDates2 = holidaysInYearForCountry2.keySet();
        holidayDates1.retainAll(holidayDates2);
        return holidayDates1.stream().filter(x -> LocalDate.parse(x).isAfter(searchStartDate))
                .sorted((o1, o2) -> LocalDate.parse(o1).compareTo(LocalDate.parse(o2))).collect(Collectors.toList());
    }

    private Optional<NextSameDateHoliday> createResponseObject(Map<String, List<Holiday>> holidayInYearForCountry1,
                                                               Map<String, List<Holiday>> holidayInYearForCountry2, List<String> matchingDates) {
        String earliestDate = matchingDates.get(0);
        String firstHolidayName = holidayInYearForCountry1.get(earliestDate).get(0).getName();
        String secondHolidayName = holidayInYearForCountry2.get(earliestDate).get(0).getName();
        return Optional.of(new NextSameDateHoliday(earliestDate, firstHolidayName, secondHolidayName));
    }
}
