package bluestone.interview.services;

import bluestone.interview.model.holidayapi.HolidayApiResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class HolidayApiService {

    private static final String HOLIDAYS_API_URL = "https://holidayapi.com/v1/holidays";
    private static final String HOLIDAYS_API_KEY_URL_PART = "?key=";
    private static final RestTemplate restTemplate = new RestTemplate();

    @Value("${holiday.api.key}")
    private String apiKey;


    public HolidayApiResponse getHolidayDataForAYear(int year, String countryCode) {
        String url = buildUrl(year, countryCode);
        return restTemplate.getForObject(url, HolidayApiResponse.class);
    }

    private String buildUrl(int year, String countryCode1) {
        return HOLIDAYS_API_URL + HOLIDAYS_API_KEY_URL_PART + apiKey + "&country=" + countryCode1 + "&year=" + year;
    }
}
