package bluestone.interview.services.rest;

import bluestone.interview.model.holidayapi.HolidayApiResponse;
import bluestone.interview.model.responses.NextSameDateHoliday;
import bluestone.interview.services.HolidayApiService;
import bluestone.interview.services.HolidayMatcherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Optional;

@RestController
public class HolidayRestService {

    private final HolidayApiService holidayApiService;

    private final HolidayMatcherService holidayMatcherService;

    @Autowired
    public HolidayRestService(HolidayApiService holidayApiService, HolidayMatcherService holidayMatcherService) {
        this.holidayApiService = holidayApiService;
        this.holidayMatcherService = holidayMatcherService;
    }

    /**
     * Service for finding from specific date closest holidays between two countries with matching date
     * Due to Holidays API limitations it searches only previous years from current one
     * @param countryCode1 first country code
     * @param countryCode2 second country code
     * @param date from which to search
     * @return Holiday names in both countries together with date or empty response body if none found
     */
    @RequestMapping(value = "/commonHolidays", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody NextSameDateHoliday commonHolidays(@RequestParam(value = "countryCode1") String countryCode1,
                                       @RequestParam(value = "countryCode2") String countryCode2,
                                       @RequestParam(value = "date") String date) {

        LocalDate searchStartDate = LocalDate.parse(date);
        int currentYear = LocalDate.now().getYear();
        int yearToSearch = searchStartDate.getYear();
        while (yearToSearch < currentYear) {
            Optional<NextSameDateHoliday> nextSameDateHoliday = searchForSameDateHolidayForYear(countryCode1, countryCode2, yearToSearch, searchStartDate);
            if (nextSameDateHoliday.isPresent()) {
                return nextSameDateHoliday.get();
            }
            yearToSearch += 1;
        }

        return null;
    }

    private Optional<NextSameDateHoliday> searchForSameDateHolidayForYear(String countryCode1, String countryCode2, int year, LocalDate searchStartDate) {
        HolidayApiResponse holidayInYearForCountry1 = holidayApiService.getHolidayDataForAYear(year, countryCode1);
        HolidayApiResponse holidayInYearForCountry2 = holidayApiService.getHolidayDataForAYear(year, countryCode2);

        return holidayMatcherService.findNextSameDateHoliday(holidayInYearForCountry1.getHolidays(), holidayInYearForCountry2.getHolidays(), searchStartDate);
    }





}
